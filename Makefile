# (C) 2022 Quazgar <quazgar@posteo.de>
#
# License: CC-0

default:
	@echo "Type 'make install' to install the file."
.PHONY: default

install:
	@bash -c '[[ -d ~/.config ]] && mkdir -p ~/.config/inkscape/symbols/ && cp src/elektroinstallation_de.svg ~/.config/inkscape/symbols/'
	@echo "Successfully installed, open Inkscape and see if the symbols are available."
.PHONY: install
