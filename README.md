# Inkscape-Symbole für die Elektroinstallation #

Deutsche Symbole für Elektroinstallations-Zeichnungen.

## Installation ##

Kopiere die Datei [`elektroinstallation_de.svg`](src/elektroinstallation_de.svg) in einen
Inkscape-Symbole-Ordner, zum Beispiel (auf Linux) nach `~/.config/inkscape/symbols/`.

Auch `make install` sollte eigentlich funktionieren..

## Wie funktioniert das? ##

- https://wiki.inkscape.org/wiki/SymbolsDialog
- http://jeromebelleman.gitlab.io/posts/graphics/inksyms/

## Lizenz ##

Alle Dateien in diesem Projekt, insbesondere die SVG-Dateien, können unter der
[CC-0-Lizenz](https://creativecommons.org/publicdomain/zero/1.0/deed.de), also ohne jegliche
Einschränkungen, verwendet werden.

